#include <iostream>
#include <vector>
#include <deque>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <stdlib.h>//srand, rand
#include <time.h>

#define W_SIZE 5/////window的大小
#define F_SIZE 20////資料保存的大小 (方便起見，ansList也用這個大小)

using namespace std;


class Semaphore////實作c++11的訊號量
{
public:
    void fullSignal() {
        unique_lock<mutex> lock(mtx);
        _full = 1;
        cv.notify_one();
    }
    void emptySignal() {
        unique_lock<mutex> lock(mtx);
        _empty = 1;
        cv.notify_one();
    }
    void waitfull()
    {
        unique_lock<mutex> lock(mtx);
        cv.wait(lock, [this]{return _full == 1;});
        _full = 0;
    }
    void waitEmpty()
    {
        unique_lock<mutex> lock(mtx);
        cv.wait(lock, [this]{return _empty == 1;});
        _empty = 0;
    }
    bool _empty = 0;
    bool _full = 0;
private:
    mutex mtx;
    condition_variable cv;//如果要看懂semaphore，最好從這東西開始弄懂
};

Semaphore semaphore;//semaphore需放在生產者跟消費者共用的記憶體位置(其實目前就只是放再全域變數)

class Source
{
public:
    Source()
    {
        auto func = [this](){
            generateData();
        };
        t = new thread(func);
    }

    void generateData()
    {
        srand (time(NULL));

        while(1){
            this_thread::sleep_for (chrono::milliseconds(50));//假裝生產一筆資料要0.05秒

            int num = rand() % 100 + 1; //1~100亂數

            if(tooMuchData())
            {
                semaphore.waitEmpty();//source的資料量要爆了，等待window消耗資料，並發送代表資料有被消耗的訊號: empty signal
            }

            mtx.lock();
            stream.push_back(num);
            if(!notEnoughData())
            {
                semaphore.fullSignal();//發射代表資料足夠的訊號: full signal
            }
            mtx.unlock();
        }
    }

    void getByWindow(int* window)
    {
        for(int i = 0; i<W_SIZE; ++i)
        {
            *window = stream[i];
            window++;
        }
    }

    void popStream()
    {
        stream.pop_front();
    }

    bool notEnoughData()
    {
        return stream.size()<W_SIZE;
    }

    bool tooMuchData()
    {
        return stream.size()>=F_SIZE;
    }

private:
    thread* t;
    mutex mtx;
    deque<int> stream;
};


class Stream
{
public:
    Stream(Source* s)
    {
        source = s;
    }

    void calculate()
    {
        if(source->notEnoughData())
        {
            semaphore.waitfull();//source資料不夠window用了，等待source發送代表資料足夠的訊號: full signal
        }

        source->getByWindow(window);//get一個window

        if(source->tooMuchData())
        {
            source->popStream();
            semaphore.emptySignal(); //發送代表資料有被消耗的訊號: empty signal
        }
        else
        {
            source->popStream();
        }

        auto access = [this](){////目前只寫了一個存取器
            double sum = 0;
            for(int i = 0; i<W_SIZE; ++i)
            {
                sum += window[i];
            }
            ansList.push_back(sum/5);
            cout<<"avg = "<<ansList.back()<<endl;
            this_thread::sleep_for (chrono::milliseconds(300));//假裝計算一組資料要0.3秒
        };

        thread c1(access);//開始c1存取器的存取

        c1.join();//等存取器存取完才可以繼續移動window

        if(ansList.size() >= F_SIZE)
        {
            return;
        }
        calculate();
    }

    void printAns()
    {
        double sum = 0;
        for(int i= 0; i<ansList.size(); ++i)
        {
            sum += ansList[i];
        }
        cout<<"avg of averages = "<<sum/F_SIZE<<endl;
    }

private:
    Source* source;
    int window[W_SIZE];
    vector<double> ansList;
};



int main()
{
    cout<<"Calculate avg from source by sliding window:"<<endl;
    cout<<"--------------------------------------------"<<endl;
    Stream stream(new Source());
    stream.calculate();
    cout<<"--------------------------------------------"<<endl;
    stream.printAns();
}
